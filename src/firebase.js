import * as firebase from "firebase/app"
import "firebase/auth"
import "firebase/firestore"
import { auth } from "./auth"

const firebaseConfig = {
    apiKey: "AIzaSyBCtPhQFbl2RAN8R5KV7GS3oZR5qO7PRw8",
    authDomain: "bi-python-f1d51.firebaseapp.com",
    databaseURL: "https://bi-python-f1d51.firebaseio.com",
    projectId: "bi-python-f1d51",
    storageBucket: "",
    messagingSenderId: "544822625856",
    appId: "1:544822625856:web:e29de375eb18d9009c41a5"
}

// Client ID and API key from the Developer Console
const CLIENT_ID = "544822625856-1sfv5g56daeg2kr1t8o92mf5m4r4q8h2.apps.googleusercontent.com"

// Array of API discovery doc URLs for APIs used by the quickstart
const DISCOVERY_DOCS = [ "https://sheets.googleapis.com/$discovery/rest?version=v4" ]

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
const SCOPES = "https://www.googleapis.com/auth/spreadsheets"

class FirebaseApp {
    constructor() {
        this.firebaseApp = firebase.initializeApp(firebaseConfig)
        gapi.load("client", () => {
            gapi.client.init({
                "apiKey": firebaseConfig.apiKey,
                // Your API key will be automatically added to the Discovery Document URLs.
                "discoveryDocs": DISCOVERY_DOCS,
                // clientId and scope are optional if auth is not required.
                "clientId": CLIENT_ID,
                "scope": SCOPES,
            })
            }
        )
        this.firebaseApp.auth().onAuthStateChanged((user) => {
            console.debug(gapi.auth2.getAuthInstance().currentUser.get())
            console.debug(user)
            auth.set({ user })
        })
        this.firebaseApp.auth().useDeviceLanguage()
        this.authProvider = new firebase.auth.GoogleAuthProvider()
        this.authProvider.addScope("https://www.googleapis.com/auth/spreadsheets.readonly")
        this.db = firebase.firestore()
    }


    loginWithGoogle() {
        return this.firebaseApp.auth().signInWithPopup(this.authProvider).then(result => {
            // This gives you a Google Access Token. You can use it to access the Google API.
            const token = result.credential.accessToken
            // The signed-in user info.
            const user = result.user
            return {
                user, token
            }
        }).catch(function (error) {
            console.error(error)
        })
    }

    logout() {
        this.firebaseApp.auth().signOut().then(() => {
            console.debug("Sign-out successful")
        }).catch(error => {
            console.error(error)
        })
    }
}

export const firebaseApp = new FirebaseApp()


export async function login() {
    firebaseApp.loginWithGoogle().then(({ user, token }) => {
        console.debug(user)
        auth.set({ user })
        gapi.client.setToken(token)
    })
}